#!/usr/bin/python3
# -*- coding: utf-8 -*-
# blockchain

import binascii


from ecdsa import SigningKey, SECP256k1


class Wallet:
    """basic keys wallet"""

    def __init__(self):

        self.private_key = None
        self.public_key = None

    def generate_key_pair(self):
        """Generate private and public keys pair"""

        sk = SigningKey.generate(curve=SECP256k1)
        self.private_key = binascii.b2a_hex(sk.to_string()).decode()
        self.public_key = binascii.b2a_hex(sk.get_verifying_key().to_string()).decode()