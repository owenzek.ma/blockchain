#!/usr/bin/python3
# -*- coding: utf-8 -*-
# blockchain

from Block import Block


import json

from Crypto.Hash import SHA256
from datetime import datetime


class Chain:
    """A chain of blocks"""

    def __init__(self):
        self.difficulty_bits = 0
        self.max_nonce = 2 ** 32  # 4 billion
        self._chain = [self.get_first_block()]

    @property
    def chain(self):
        """return list of block objects dict"""

        return self.dict(self._chain)

    def dict(self, chain):
        """converts list of block objects to dictionary"""

        return json.loads(json.dumps(chain, default=lambda o: o.__dict__))

    def reset(self):
        """resets the chain blocks except the first one block"""

        self._chain = [self._chain[0]]

    def get_first_block(self):
        """creates first block of the chain"""

        index = 0
        previous_hash = "0"
        timestamp = int(datetime.now().timestamp())
        data = 'First block'
        first_block_hash, nonce = self.calculate_hash(index, previous_hash, timestamp, data)

        block = Block(
            index,
            previous_hash,
            timestamp,
            data,
            0,
            0,
            first_block_hash
        )

        return block

    def add_block(self, data):
        """appends a new block to the blockchain"""

        self._chain.append(self.create_block(data))

    def create_block(self, block_data):
        """creates a new block with the given block data"""

        previous_block = self.get_latest_block()
        next_index = previous_block.index + 1
        next_timestamp = int(datetime.now().timestamp())
        next_hash, next_nonce = self.calculate_hash(next_index, previous_block.hash, next_timestamp, block_data)

        block = Block(
            next_index,
            previous_block.hash,
            next_timestamp,
            block_data,
            self.difficulty_bits,
            next_nonce,
            next_hash
        )

        return block

    def get_latest_block(self):
        """gets the last block from the blockchain"""

        try:
            return self._chain[-1]
        except IndexError as e:
            return None

    def calculate_hash(self, index, previous_hash, timestamp, data):
        """calculates SHA256 hash value by solving hash puzzle"""

        header = str(index) + previous_hash + str(timestamp) + data + str(self.difficulty_bits)

        hash_value, nonce = self.proof_of_work(header)
        return hash_value, nonce

    def proof_of_work(self, header):

        target = 2 ** (256 - self.difficulty_bits)

        for nonce in range(self.max_nonce):
            hash_result = SHA256.new(data=(str(header) + str(nonce)).encode()).hexdigest()

            if int(hash_result, 16) < target:
                print("Preuve trouvée avec le nonce %d" % nonce)
                print("Haché: %s" % hash_result)
                return hash_result, nonce

        print("Failed after %d (max_nonce) tries" % self.max_nonce)
        return self.max_nonce




