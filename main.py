#!/usr/bin/python3
# -*- coding: utf-8 -*-
# blockchain


import json
import time

from datetime import datetime


from Chain import Chain
from Transaction import Transaction
from UnmutableChain import UnmutableChain
from Wallet import Wallet


def add_blocks(chain):
    time.sleep(1)
    chain.add_block("Premier bloc")
    time.sleep(1)
    chain.add_block("Second bloc")
    time.sleep(1)
    chain.add_block("Troisieme bloc")

first_chain = UnmutableChain()
add_blocks(first_chain)
print(json.dumps(first_chain.chain) + '\n')

first_chain.reset()
add_blocks(first_chain)
print(json.dumps(first_chain.chain) + '\n')
print('===============\n\n\n\n')


time.sleep(1)

second_chain = Chain()
add_blocks(second_chain)
print(json.dumps(second_chain.chain) + '\n')

second_chain.reset()
add_blocks(second_chain)
print(json.dumps(second_chain.chain) + '\n')
print('===============\n\n\n\n')


account1 = Wallet()
account1.generate_key_pair()
print("Clef publique pair 1: %s" % account1.public_key)
print("Clef privee pair 1: %s" % account1.private_key)

account2 = Wallet()
account2.generate_key_pair()
print("Clef publique pair 2: %s" % account2.public_key)
print("Clef privee pair 2: %s" % account2.private_key)

tx = Transaction(account1.public_key)
tx.sign(account1.private_key, 'Test')
print("Signature générée: %s" % tx.signature)
tx.verify()

tx.id = 1234
tx.verify()
print('===============\n\n\n\n')

print("Clef publique pair 1: %s" % account1.public_key)
print("Clef privee pair 1: %s" % account1.private_key)
print("Clef publique pair 2: %s" % account2.public_key)
print("Clef privee pair 2: %s" % account2.private_key)
print('\n')
transaction_chain = Chain()
difficulty_bits = 8
difficulty = 2 ** difficulty_bits
transaction_chain.difficulty_bits = difficulty_bits
print('---\n')
time.sleep(1)

tx1 = Transaction(account1.public_key)
tx1.sign(account1.private_key, "Premier bloc")
transaction_chain.add_block(tx1.to_json())
print('---\n')
time.sleep(1)

tx2 = Transaction(account2.public_key)
tx2.sign(account2.private_key, "Second bloc")
transaction_chain.add_block(tx2.to_json())
print('---\n')
time.sleep(1)

tx3 = Transaction(account1.public_key)
tx3.sign(account1.private_key, "Troisieme bloc")
transaction_chain.add_block(tx3.to_json())
print('---\n')
time.sleep(1)

print(json.dumps(transaction_chain.chain) + '\n')
print('===============\n\n\n\n')

nonce_chain = Chain()

index = 0

for difficulty_bits in range(20):

    difficulty = 2 ** difficulty_bits
    nonce_chain.difficulty_bits = difficulty_bits

    print("Difficulté: %ld (%d bits)" % (difficulty, difficulty_bits))
    print("Début de la recherche...")

    start_time = datetime.now()

    new_block_data = 'bloc de test #%d' % index
    nonce_chain.add_block(data=new_block_data)

    end_time = datetime.now()

    elapsed_time = (end_time - start_time).total_seconds()
    print("Temps écoulé : %.4f secondes" % elapsed_time)

    if elapsed_time > 0:

        hash_power = float(int(nonce_chain.chain[-1].get("nonce")) / elapsed_time)
        print("Puissance de hachage: %ld hachés par second" % hash_power)

    print('-----  \n')
    index = index+1