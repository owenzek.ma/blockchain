#!/usr/bin/python3
# -*- coding: utf-8 -*-
# blockchain

import binascii


from Crypto.Hash import SHA256
from Crypto.Cipher import PKCS1_OAEP
# from Crypto.PublicKey import RSA
from ecdsa import SigningKey, VerifyingKey, SECP256k1, keys
from random import randint


class Transaction:

    def __init__(self, public_key):
        self.id = randint(1, 10**5)
        self.signature = None
        self.public_key = public_key
        self.payload = None

    def verify(self):

        vk = VerifyingKey.from_string(bytes.fromhex(self.public_key), curve=SECP256k1)
        data_to_check = SHA256.new(str(self.id).encode()).digest()

        try:
            vk.verify(bytes.fromhex(self.signature), data_to_check)

        except keys.BadSignatureError:
            print('Transaction : signature invalide')
            return False

        print('Transaction : signature valide')
        return True

    def sign(self, private_key, data):

        data_to_sign = SHA256.new(str(self.id).encode()).digest()
        sk = SigningKey.from_string(bytes.fromhex(private_key), curve=SECP256k1)
        self.signature = binascii.b2a_hex(sk.sign(data_to_sign)).decode()
        self.payload = data

    def to_json(self):
        return '{id:' + str(self.id) + ', signature:' + str(self.signature) + ', payload:' + str(self.payload) + '}'
