#!/usr/bin/python3
# -*- coding: utf-8 -*-
# blockchain


class Block:
    """A block for the blockchain"""

    def __init__(self, index, previous_hash, timestamp, data,
                 difficulty_bits, nonce, hash):
        self.index = index
        self.previous_hash = previous_hash
        self.timestamp = timestamp
        self.data = data
        self.difficulty_bits = difficulty_bits
        self.nonce = nonce
        self.hash = hash