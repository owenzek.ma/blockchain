#!/usr/bin/python3
# -*- coding: utf-8 -*-
# blockchain
from Block import Block
from Chain import Chain


import json

from Crypto.Hash import SHA256
from datetime import datetime


class UnmutableChain(Chain):
    """A chain of blocks"""

    def __init__(self):
        self.timestamp = int(datetime.now().timestamp())
        self.difficulty_bits = 0
        self.max_nonce = 0
        self._chain = [self.get_first_block()]

    def get_first_block(self):
        """creates first block of the chain"""

        index = 0
        previous_hash = "0"
        data = 'First block'
        first_block_hash = self.calculate_hash(index, previous_hash, self.timestamp, data)

        print(first_block_hash)

        block = Block(
            index,
            previous_hash,
            self.timestamp,
            data,
            0,
            0,
            first_block_hash
        )

        return block

    def create_block(self, block_data):
        """creates a new block with the given block data"""

        previous_block = self.get_latest_block()
        next_index = previous_block.index + 1
        next_timestamp = self.timestamp
        next_hash = self.calculate_hash(next_index, previous_block.hash, next_timestamp, block_data)

        block = Block(
            next_index,
            previous_block.hash,
            next_timestamp,
            block_data,
            0,
            0,
            next_hash
        )

        return block

    def calculate_hash(self, index, previous_hash, timestamp, data):
        """calculates SHA256 hash value"""

        hash_object = SHA256.new(data=(str(index) + previous_hash + str(timestamp) + data).encode())
        return hash_object.hexdigest()




